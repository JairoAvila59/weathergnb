package com.example.weathergnb.data.remote

import com.example.weathergnb.data.remote.locationModel.LocationResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteGeocodingService {

    @GET("json")
    fun requestCityAddressByName(
        @Query("address") address: String,
        @Query("key") key: String
    ): Single<LocationResponse>
}