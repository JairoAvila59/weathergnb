package com.example.weathergnb.data.repository

import io.reactivex.Flowable
import io.reactivex.Single

interface WeatherRepository {

    fun getCities(): Int

    fun getWeather(cityName: String):Int

    fun addCity(cityName: String)
}