package com.example.weathergnb.data.repository

import com.example.weathergnb.data.remote.RemoteWeatherDataSource
import com.example.weathergnb.data.room.RoomDataSource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepositoryImpl @Inject constructor(
    private val remoteWeatherDataSource: RemoteWeatherDataSource,
    private val roomDataSource: RoomDataSource
) : WeatherRepository {

    override fun getCities(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getWeather(cityName: String): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun addCity(cityName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}