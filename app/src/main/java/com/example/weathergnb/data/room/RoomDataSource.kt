package com.example.weathergnb.data.room

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase

abstract class RoomDataSource : RoomDatabase() {

    companion object {

        @Volatile private var INSTANCE: RoomDataSource? = null

        fun getInstance(context: Context): RoomDataSource =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext,
                RoomDataSource::class.java, RoomConfig.DATABASE_WEATHER)
                .build()
    }
}