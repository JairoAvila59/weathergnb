package com.example.weathergnb.di

import com.example.weathergnb.ui.WeatherSearchActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppModule::class, RemoteModule::class, RoomModule::class, WeatherRepositoryModule::class])
@Singleton
interface AppComponent {

    fun inject(weatherSearchActivity: WeatherSearchActivity)
}