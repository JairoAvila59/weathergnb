package com.example.weathergnb.di

import android.content.Context
import com.example.weathergnb.WeatherApplication
import com.example.weathergnb.data.repository.WeatherRepository
import com.example.weathergnb.ui.WeatherViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val weatherApplication: WeatherApplication) {

    @Provides
    @Singleton
    fun provideContext(): Context = weatherApplication

    @Provides
    @Singleton
    fun provideViewModelFactory(weatherRepository: WeatherRepository): WeatherViewModelFactory {
        return WeatherViewModelFactory(weatherRepository)
    }

}