package com.example.weathergnb.di

import com.example.weathergnb.data.repository.WeatherRepository
import com.example.weathergnb.data.repository.WeatherRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class WeatherRepositoryModule {
    @Binds
    abstract fun bindsWeatherRepository(weatherRepositoryImpl: WeatherRepositoryImpl): WeatherRepository
}